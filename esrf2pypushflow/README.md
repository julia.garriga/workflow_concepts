# esrf2pypushflow

*esrftaskgraph* binding for the task scheduler of *pypushflow*. This task scheduler allows the execution of cyclic and acyclic graphs and uses python's *multiprocessing* for task distribution.

## Graph of actors

An ESRF task graph is converted to a graph of *actors*.

### Source

A task is wrapped by an *EsrfPythonActor* followed by some binder actors, depending on the type of link between source and target. For each link in the task graph, the final actor of the source is a *NameMapperActor* which is connected to the target.

#### Unconditional link
```mermaid
graph LR;
    EsrfPythonActor-->NameMapperActor;
```

#### Conditional link with one condition
```mermaid
graph LR;
    EsrfPythonActor-->DecodeRouterActor;
    DecodeRouterActor-->NameMapperActor;
```

#### Conditional link with multiple conditions
```mermaid
graph LR;
    EsrfPythonActor-->DecodeRouterActor1;
    EsrfPythonActor-->DecodeRouterActor2;
    DecodeRouterActor1-->JoinActor;
    DecodeRouterActor2-->JoinActor;
    JoinActor-->NameMapperActor;
```

### Target

The *NameMapperActor* of a source is connected to an *EsrfPythonActor* (1 upstream task) or an *InputMergeActor* (>1 upstream tasks).

### Start actor

The *StartActor* is connected to all tasks that satisfy any of these conditions:
 * no predecessors
 * only conditional predecessors and all required inputs specified in the node attribute

### Stop actor

Tasks that satisfy any of these conditions are connected to the *InputMergeActor* preceeding the *StopActor*:
 * no successors
 * only conditional successors (routers without a default successor are connected)

### Custom actors

These actors are not provided by *pypushflow*

* *EsrfPythonActor*: like *PythonActor* but it passes node name and attributes to the next actor.
* *InputMergeActor*: like *Joinactor* (merges the input data dictionaries) but it triggers the downstream actors when the number of inputs is greater or equal than the required number of inputs.
* *DecodeRouterActor*: line *RouterActor* but it dereferences thet input hashes to get the values.
* *NameMapperActor*: before triggering the next task it applies filtering and name mapping to the input data.
