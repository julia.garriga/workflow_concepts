import logging
from esrftaskgraph import UniversalHash
from esrftaskgraph import instantiate_task


INFOKEY = "__noninput__"


logger = logging.getLogger(__name__)


def run(**kw):
    """Main of actor execution.

    :param **kw: output hashes from previous tasks
    :returns dict: output hashes
    """
    info = kw.pop(INFOKEY)
    inputs = {name: UniversalHash(uhash) for name, uhash in kw.items()}
    task = instantiate_task(info["node_attrs"], varinfo=info["varinfo"], inputs=inputs)

    try:
        task.run()
    except Exception as e:
        logger.error(
            "\nEXECUTE {} {}\n INPUTS: {}\n ERROR: {}".format(
                info["node_name"],
                repr(task),
                task.input_values,
                e,
            ),
        )
        raise

    logger.info(
        "\nEXECUTE {} {}\n INPUTS: {}\n OUTPUTS: {}".format(
            info["node_name"],
            repr(task),
            task.input_values,
            task.output_values,
        ),
    )
    return task.output_uhashes
