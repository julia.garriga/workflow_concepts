import sys
import logging
from esrf2pypushflow import job
from taskgraphlib import check_pipeline

logging.getLogger("esrf2pypushflow").setLevel(logging.DEBUG)
logging.getLogger("esrf2pypushflow").addHandler(logging.StreamHandler(sys.stdout))


def workflow2():
    nodes = [
        {
            "id": "Python Error Handler Test",
            "inputs": {"name": "myname"},
            "method": "pypushflow.test.pythonErrorHandlerTest.run",
        },
    ]

    links = []

    graph = {
        "directed": True,
        "graph": {"name": "workflow2"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    # Eplicit check that the task didn't finish successfully
    expected_results = {"Python Error Handler Test": None}

    return graph, expected_results


def test_workflow2(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = workflow2()
    result = job(graph, varinfo=varinfo, raise_on_error=False)
    check_pipeline(graph, expected, varinfo)
    err_msg = "Runtime error in pythonErrorHandlerTest.py!"
    assert result["WorkflowException"]["errorMessage"] == err_msg
