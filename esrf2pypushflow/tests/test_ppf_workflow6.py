import sys
import logging
from esrf2pypushflow import job
from taskgraphlib import check_pipeline
from esrftaskgraph import merge_graphs

logging.getLogger("esrf2pypushflow").setLevel(logging.DEBUG)
logging.getLogger("esrf2pypushflow").addHandler(logging.StreamHandler(sys.stdout))


def submodel6():
    nodes = [
        {
            "id": "addtask",
            "method": "pypushflow.test.pythonActorAdd.run",
            "input_names": ["value"],
            "output_names": ["value"],
        },
    ]

    links = []

    graph = {
        "directed": True,
        "graph": {"name": "submodel6"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    return graph


def workflow6():
    nodes = [
        {
            "id": "addtask1",
            "inputs": {"value": 1},
            "method": "pypushflow.test.pythonActorAdd.run",
            "input_names": ["value"],
            "output_names": ["value"],
        },
        {
            "id": "addtask3",
            "method": "pypushflow.test.pythonActorAdd.run",
            "input_names": ["value"],
            "output_names": ["value"],
        },
        {"id": "submodel6", "graph": submodel6()},
    ]

    links = [
        {
            "source": "addtask1",
            "target": "submodel6",
            "links": [
                {
                    "source": "addtask1",
                    "target": "addtask",
                    "all_arguments": True,  # implicit mapping
                }
            ],
        },
        {
            "source": "submodel6",
            "target": "addtask3",
            "links": [
                {
                    "source": "addtask",
                    "target": "addtask3",
                    "arguments": {"value": "value"},  # explicit mapping
                }
            ],
        },
    ]

    graph = {
        "directed": True,
        "graph": {"name": "workflow6"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    expected_results = {
        ("workflow6", "addtask1"): {"value": 2},
        ("submodel6", "addtask"): {"value": 3},
        ("workflow6", "addtask3"): {"value": 4},
    }

    return graph, expected_results


def test_workflow6(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = workflow6()
    job(graph, varinfo=varinfo)
    check_pipeline(graph, expected, varinfo)
