import sys
import logging
from esrf2pypushflow import job
from taskgraphlib import check_pipeline

logging.getLogger("esrf2pypushflow").setLevel(logging.DEBUG)
logging.getLogger("esrf2pypushflow").addHandler(logging.StreamHandler(sys.stdout))


def workflow1():
    nodes = [
        {
            "id": "Python Actor Test",
            "inputs": {"name": "myname"},
            "method": "pypushflow.test.pythonActorTest.run",
        },
    ]

    links = []

    graph = {
        "directed": True,
        "graph": {"name": "workflow1"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    expected_results = {
        "Python Actor Test": {"return_value": {"reply": "Hello myname!"}}
    }

    return graph, expected_results


def test_workflow1(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = workflow1()
    job(graph, varinfo=varinfo)
    check_pipeline(graph, expected, varinfo)
