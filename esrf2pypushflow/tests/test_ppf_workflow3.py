import sys
import logging
from esrf2pypushflow import job
from taskgraphlib import check_pipeline
from esrftaskgraph import merge_graphs

logging.getLogger("esrf2pypushflow").setLevel(logging.DEBUG)
logging.getLogger("esrf2pypushflow").addHandler(logging.StreamHandler(sys.stdout))


def submodel1():
    nodes = [
        {
            "id": "mytask",
            "inputs": {"name": "myname"},
            "method": "pypushflow.test.pythonActorTest.run",
        },
    ]

    links = []

    graph = {
        "directed": True,
        "graph": {"name": "submodel1"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    return graph


def workflow3():
    nodes = [
        {
            "id": "first",
            "inputs": {"name": "first"},
            "method": "pypushflow.test.pythonActorTest.run",
        },
        {
            "id": "last",
            "inputs": {"name": "last"},
            "method": "pypushflow.test.pythonActorTest.run",
        },
        {"id": "middle", "graph": submodel1()},
    ]

    links = [
        {
            "source": "first",
            "target": "middle",
            "links": [
                {
                    "source": "first",
                    "target": "mytask",
                    "node_attributes": {"inputs": {"name": "middle"}},
                }
            ],
        },
        {
            "source": "middle",
            "target": "last",
            "links": [
                {
                    "source": "mytask",
                    "target": "last",
                    "node_attributes": "<not-used>",
                }
            ],
        },
    ]

    graph = {
        "directed": True,
        "graph": {"name": "workflow3"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    expected_results = {
        ("workflow3", "first"): {"return_value": {"reply": "Hello first!"}},
        ("middle", "mytask"): {"return_value": {"reply": "Hello middle!"}},
        ("workflow3", "last"): {"return_value": {"reply": "Hello last!"}},
    }

    return graph, expected_results


def test_workflow3(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = workflow3()
    job(graph, varinfo=varinfo)
    check_pipeline(graph, expected, varinfo)
