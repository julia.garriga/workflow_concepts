import sys
from Orange.canvas.__main__ import main as _main


def main(**kw):
    # Register an addon library with wasn't registered at runtime.
    # import orange3unregistered
    # from esrf2orange3.registration import register_addon_package
    # register_addon_package(orange3unregistered, distroname="orange3widgetlib")
    _main(**kw)


if __name__ == "__main__":
    sys.exit(main())
