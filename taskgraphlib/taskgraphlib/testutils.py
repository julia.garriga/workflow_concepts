from esrftaskgraph import load_graph


def check_pipeline(taskgraph, expected, varinfo):
    tasks = dict()
    taskgraph = load_graph(taskgraph)
    for node in taskgraph.graph.nodes:
        task = taskgraph.instantiate_task_static(node, tasks=tasks, varinfo=varinfo)
        value = expected.get(node)
        if value is None:
            assert not task.done, node
        else:
            assert task.done, node
            try:
                assert task.output.value == value, node
            except AssertionError:
                raise
            except Exception as e:
                raise RuntimeError(f"{node} does not have a result") from e
