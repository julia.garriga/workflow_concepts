# taskgraph

Example of a project that defines graphs based on task libraries deriving from *esrftaskgraph*. Or rather it defines *pipelines* which as graph instances. A graph instance is a graph with fixed static inputs.
