import os
from glob import glob
import testbook


def test_notebooks():
    root_dir = os.path.dirname(os.path.dirname(__file__))
    notebooks = glob(os.path.join(root_dir, "*.ipynb"))
    for filename in notebooks:
        with testbook.testbook(filename, execute=True):
            pass
