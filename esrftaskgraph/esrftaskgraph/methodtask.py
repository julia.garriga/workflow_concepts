import importlib
from esrftaskgraph.task import Task


class _MethodExecutorTask(Task, is_abstract=True):
    INPUT_NAMES = Task.INPUT_NAMES + ["method"]

    def _execute_method(self):
        fullname = self.input.method.value
        if not isinstance(fullname, str):
            raise TypeError(fullname, type(fullname))
        module_name, sep, method_name = fullname.rpartition(".")
        if not sep:
            raise TypeError("Method needs a module to import from")
        module = importlib.import_module(module_name)

        try:
            method = getattr(module, method_name)
        except AttributeError:
            raise ImportError(f"cannot import {method_name} from {module_name}")
        if not callable(method):
            raise RuntimeError(repr(fullname) + " is not callable")

        kwargs = {k: var.value for k, var in self.input.items() if k != "method"}
        return method(**kwargs)


class MethodExecutorTask(_MethodExecutorTask):
    """Only one return value and one required input, the method
    you want to execute.
    """

    OUTPUT_NAMES = _MethodExecutorTask.OUTPUT_NAMES + ["return_value"]

    def process(self):
        self.output.return_value.value = self._execute_method()


class UnpackedMethodExecutorTask(_MethodExecutorTask):
    """Unpacks the result of the method (must be dict-like)."""

    def __init__(self, input_names=None, output_names=None, **kw):
        if isinstance(input_names, list) and input_names:
            self.INPUT_NAMES = self.INPUT_NAMES + input_names
        self._unpack_output = isinstance(output_names, list) and output_names
        if self._unpack_output:
            self.OUTPUT_NAMES = self.OUTPUT_NAMES + output_names
        else:
            self.OUTPUT_NAMES = self.OUTPUT_NAMES + ["return_value"]
        super().__init__(**kw)

    def process(self):
        result = self._execute_method()
        if self._unpack_output:
            self.output.update_values(result)
        else:
            self.output.return_value.value = result
