from esrftaskgraph import hashing
from esrftaskgraph.variable import VariableContainer
from esrftaskgraph.registration import Registered


class TaskInputError(RuntimeError):
    pass


class Task(hashing.UniversalHashable, metaclass=Registered, is_abstract=True):
    """Node in a task Graph with named inputs and outputs.

    The universal hash of the task is equal to the universal
    hash of the output. The universal hash of the output is
    equal to the hash of the inputs and the task nonce.

    A task is done when its output exists.

    This is an abstract class. Instantiating a `Task` should be
    done with `esrftaskgraph.inittask.instantiate_task`.
    """

    INPUT_NAMES = []
    OPTIONAL_INPUT_NAMES = []
    OUTPUT_NAMES = []

    def __init__(self, varinfo=None, **inputs):
        """The named arguments are inputs and Variable configuration"""
        self.validate()

        # Check required inputs
        missing_required = set(self.INPUT_NAMES) - set(inputs.keys())
        if missing_required:
            raise ValueError(f"Missing inputs for {type(self)}: {missing_required}")

        # Init missing optional inputs
        missing_optional = set(self.OPTIONAL_INPUT_NAMES) - set(inputs.keys())
        for name in missing_optional:
            inputs[name] = None

        # Required outputs for the task to be "done"
        ovars = {name: None for name in self.OUTPUT_NAMES}

        # Misc
        self._exception = None

        # The output hash will update dynamically if any of the input
        # variables change
        self._input = VariableContainer(value=inputs, **varinfo)
        self._output = VariableContainer(
            value=ovars, **varinfo, uhash=self._input, uhash_nonce=self._class_nonce
        )

        # The task class has the same hash as its output
        super().__init__(uhash=self._output)

    @classmethod
    def validate(cls):
        names = cls.input_names()
        if len(names) != len(set(names)):
            raise ValueError("Duplicate input names")

    @classmethod
    def instantiate(cls, name, **kw):
        """Factory method for instantiating a derived class.

        :param str name: for example "tasklib.tasks.SumTask" or "SumTask"
        :param **kw: `Task` constructor arguments
        :returns Task:
        """
        return cls.get_subclass(name)(**kw)

    @classmethod
    def input_names(cls):
        return cls.INPUT_NAMES + cls.OPTIONAL_INPUT_NAMES

    @classmethod
    def output_names(cls):
        return cls.OUTPUT_NAMES

    def __repr__(self):
        return f"{type(self).__qualname__}({self.uhash})"

    @property
    def input(self):
        return self._input

    @property
    def input_uhashes(self):
        return self._input.variable_uhashes

    @property
    def input_values(self):
        return self._input.variable_values

    @property
    def output(self):
        return self._output

    @property
    def output_uhashes(self):
        return self._output.variable_uhashes

    @property
    def output_values(self):
        return self._output.variable_values

    @property
    def done(self):
        """Completed (with or without exception)"""
        return self.failed or self.output.exists

    @property
    def failed(self):
        return self._exception is not None

    @property
    def can_run(self):
        for iname in self.INPUT_NAMES:
            ivar = self.input.get(iname)
            if ivar is None or not ivar.available:
                return False
        return True

    def check_can_run(self):
        unavailable = list()
        for iname in self.INPUT_NAMES:
            ivar = self.input.get(iname)
            if ivar is None or not ivar.available:
                unavailable.append(iname)
        if unavailable:
            raise TaskInputError(
                "The following inputs could not be loaded: " + str(unavailable)
            )

    def run(self, force_rerun=False, persistent_output=True, raise_on_error=True):
        try:
            if force_rerun:
                # Rerun a task which is already done
                self.output.force_non_existing()
            if self.done:
                return
            self.check_can_run()
            self.process()
            if persistent_output:
                # Persistency is required for task scheduling
                # without data transfer
                self.output.dump()
        except Exception as e:
            self._exception = e
            if raise_on_error:
                raise

    def process(self):
        """To be implemented by the derived classes"""
        raise NotImplementedError
