import sys
import subprocess
from esrftaskgraph.task import Task


class ScriptExecutorTask(Task):
    INPUT_NAMES = Task.INPUT_NAMES + ["script"]
    OUTPUT_NAMES = Task.OUTPUT_NAMES + ["returncode"]

    def process(self):
        fullname = self.input.script.value
        if not isinstance(fullname, str):
            raise TypeError(fullname, type(fullname))
        args = []
        if fullname.endswith(".py"):
            argmarker = "--"
            args.append(sys.executable)
        else:
            argmarker = "-"
            args.append("bash")
        args.append(fullname)
        for k, var in self.input.items():
            if k != "script":
                args.extend((argmarker + k, str(var.value)))
        result = subprocess.run(args)
        # result.check_returncode()
        self.output.returncode.value = result.returncode
