from esrftaskgraph.task import Task
from esrftaskgraph.methodtask import MethodExecutorTask
from esrftaskgraph.methodtask import UnpackedMethodExecutorTask
from esrftaskgraph.scripttask import ScriptExecutorTask


def instantiate_task(node_attrs, varinfo=None, inputs=None):
    """
    :param dict node_attrs: node attributes of the graph representation
    :param dict varinfo: `Variable` constructor arguments
    :param dict or None inputs: dynamic inputs (from other tasks)
    :returns Task:
    """
    # Static inputs
    kwargs = dict(node_attrs.get("inputs", dict()))
    # Dynamic inputs (from other tasks)
    if inputs:
        kwargs.update(inputs)
    # `Variable` arguments
    kwargs["varinfo"] = varinfo

    # Instantiate task
    task_class = node_attrs.get("class")
    if task_class:
        return Task.instantiate(task_class, **kwargs)
    method = node_attrs.get("method")
    if method:
        input_names = node_attrs.get("input_names")
        output_names = node_attrs.get("output_names")
        if input_names or output_names:
            return UnpackedMethodExecutorTask(
                input_names=input_names,
                output_names=output_names,
                method=method,
                **kwargs
            )
        else:
            return MethodExecutorTask(method=method, **kwargs)
    script = node_attrs.get("script")
    if script:
        return ScriptExecutorTask(script=script, **kwargs)
    raise ValueError("Task requires the 'class', 'method' or 'script' key")


def get_task_class(node_attrs):
    task_class = node_attrs.get("class")
    if task_class:
        return Task.get_subclass(task_class)
    method = node_attrs.get("method")
    if method:
        return MethodExecutorTask
    script = node_attrs.get("script")
    if script:
        return ScriptExecutorTask
    raise ValueError("Task requires the 'class', 'method' or 'script' key")
