import random
import hashlib
from collections.abc import Mapping, Iterable, Set
import numpy
from esrftaskgraph.utils import qualname


def uhashtype(_type):
    return qualname(_type).encode()


def uhash(value, _hash=None):
    """Universial hash (as opposed to python's hash).
    This is an example. Must find something better.

    :param value:
    :param _hash: for internal recursive calls
    :returns Identifier:
    """
    # Avoid using python's hash!
    bdigest = _hash is None
    if bdigest:
        _hash = hashlib.sha256()
    _hash.update(uhashtype(type(value)))
    if value is None:
        pass
    elif isinstance(value, UniversalHashable):
        _hash.update(repr(value.uhash).encode())
    elif isinstance(value, UniversalHash):
        _hash.update(repr(value).encode())
    elif isinstance(value, bytes):
        _hash.update(value)
    elif isinstance(value, str):
        _hash.update(value.encode())
    elif isinstance(value, int):
        _hash.update(hex(value).encode())
    elif isinstance(value, float):
        _hash.update(value.hex().encode())
    elif isinstance(value, (numpy.ndarray, numpy.number)):
        _hash.update(value.tobytes())
    elif isinstance(value, Mapping):
        keys, values = zip(*sorted(value.items(), key=lambda item: item[0]))
        uhash(keys, _hash=_hash)
        uhash(values, _hash=_hash)
    elif isinstance(value, Set):
        # Unordered
        raise TypeError(value, type(value))
    elif isinstance(value, Iterable):
        # Ordered
        for v in value:
            uhash(v, _hash=_hash)
    else:
        raise TypeError(value, type(value))
    if bdigest:
        return UniversalHash(_hash.hexdigest())


class UniversalHash:
    def __init__(self, hexdigest):
        if isinstance(hexdigest, bytes):
            hexdigest = hexdigest.decode()
        if not isinstance(hexdigest, str):
            raise TypeError(hexdigest, type(hexdigest))
        self._hexdigest = hexdigest

    def __hash__(self):
        # make it python hashable (to use in sets and dict keys)
        return hash(repr(self))

    def __repr__(self):
        return "UniversalHash('{}')".format(self)

    def __str__(self):
        return self._hexdigest

    def __eq__(self, other):
        return str(self) == str(other)

    def __lt__(self, other):
        return str(self) < str(other)


class UniversalHashable:
    """The univerial hash is either:

    * an external hash optionally mixed with a instance nonce
    * a hash of internal data, mixed with class name, class version and instance nonce
    * `None` in case the external hash and internal data are `None`
    """

    VERSION = tuple()

    def __init__(self, uhash=None, uhash_nonce=None):
        """
        :param str, bytes, UniversalHash, UniversalHashable uhash:
        :param uhash_nonce:
        """
        self.__uhash = None
        self.__nonce = uhash_nonce
        if uhash is None:
            pass
        elif isinstance(uhash, (str, bytes)):
            self.__uhash = UniversalHash(uhash)
        elif isinstance(uhash, (UniversalHash, UniversalHashable)):
            self.__uhash = uhash
        else:
            raise TypeError(uhash, type(uhash))

    @property
    def _class_nonce(self):
        return uhashtype(type(self)), self.VERSION

    @property
    def _instance_nonce(self):
        return self.__nonce

    @property
    def uhash(self):
        if self.__uhash is None:
            data = self._uhash_data()
            if data is None:
                return None
            if self._instance_nonce is None:
                return uhash((data, self._class_nonce))
            else:
                return uhash((data, self._class_nonce, self._instance_nonce))
        else:
            _uhash = self.__uhash
            if isinstance(_uhash, UniversalHashable):
                _uhash = _uhash.uhash
            if self._instance_nonce is None:
                return _uhash
            else:
                return uhash((_uhash, self._instance_nonce))

    def _uhash_data(self):
        return None

    def _uhash_randomize(self):
        self.__nonce = random.randint(-1e100, 1e100)

    def __hash__(self):
        # make it python hashable (to use in sets and dict keys)
        return hash(self.uhash)

    def __eq__(self, other):
        if isinstance(other, UniversalHashable):
            uhash = other.uhash
        elif isinstance(other, UniversalHash):
            uhash = other
        else:
            raise TypeError(other, type(other))
        return self.uhash == uhash
