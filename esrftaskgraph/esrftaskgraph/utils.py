import importlib


def qualname(_type):
    mod_name = _type.__module__
    if not mod_name or mod_name == str.__module__:
        return _type.__name__  # Avoid reporting builtins
    else:
        return mod_name + "." + _type.__name__


def import_qualname(qualname):
    module_name, dot, class_name = qualname.rpartition(".")
    if not module_name:
        raise ImportError(f"cannot import {qualname}")
    module = importlib.import_module(module_name)
    try:
        return getattr(module, class_name)
    except AttributeError:
        raise ImportError(f"cannot import {class_name} from {module_name}")
