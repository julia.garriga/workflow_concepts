import os
import pytest
import json
from esrftaskgraph.utils import qualname
from esrftaskgraph.task import Task
import esrftaskgraph.methodtask
from tasklib.tasks import SumTask


def mymethod(a=0, b=0):
    return {"result": a + b}


def assert_storage(tmpdir, expected):
    lst = []
    for fileobj in tmpdir.listdir():
        lst.append(json.load(fileobj))
    for v in lst:
        if isinstance(v, dict):
            v.pop("__traceback__", None)
    assert len(lst) == len(expected)
    for v in expected:
        lst.pop(lst.index(v))
    assert not lst, "Unexpected data saved"


def test_task_missing_input(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    with pytest.raises(ValueError):
        SumTask(varinfo=varinfo)


def test_task_optional_input(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    task = SumTask(a=10, varinfo=varinfo)
    assert not task.done
    task.run()
    assert task.done
    assert task.output["result"].value == 10
    expected = [{"result": str(task.output["result"].uhash)}, 10]
    assert_storage(tmpdir, expected)


def test_task_uhash(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    task = SumTask(a=10, varinfo=varinfo)
    uhash = task.uhash
    assert task.uhash == task.output.uhash
    assert task.uhash != task.input.uhash

    task.input.a.value += 1
    assert task.uhash != uhash
    assert task.uhash == task.output.uhash
    assert task.uhash != task.input.uhash
    uhash = task.uhash
    task.run()
    assert task.done

    task.input.a.value += 1
    assert task.uhash != uhash
    assert task.uhash == task.output.uhash
    assert task.uhash != task.input.uhash
    assert not task.done


def test_task_storage(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    task = SumTask(a=10, b=2, varinfo=varinfo)
    assert not task.done
    task.run()
    assert task.done
    assert task.output["result"].value == 12
    expected = [{"result": str(task.output["result"].uhash)}, 12]
    assert_storage(tmpdir, expected)

    task = SumTask(a=10, b=2, varinfo=varinfo)
    assert task.done
    assert task.output["result"].value == 12
    assert_storage(tmpdir, expected)

    task = SumTask(a=2, b=10, varinfo=varinfo)
    assert not task.done
    task.run()
    assert task.done
    assert task.output["result"].value == 12
    expected += [{"result": str(task.output["result"].uhash)}, 12]
    assert_storage(tmpdir, expected)

    task = SumTask(a=task.output["result"], b=0, varinfo=varinfo)
    assert not task.done
    task.run()
    assert task.done
    assert task.output["result"].value == 12
    expected += [{"result": str(task.output["result"].uhash)}, 12]
    assert_storage(tmpdir, expected)

    task = SumTask(a=1, b=task.output["result"].uhash, varinfo=varinfo)
    assert not task.done
    task.run()
    assert task.done
    assert task.output["result"].value == 13
    expected += [{"result": str(task.output["result"].uhash)}, 13]
    assert_storage(tmpdir, expected)


def test_method_task(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    task = Task.instantiate(
        "MethodExecutorTask", method=qualname(mymethod), a=3, b=5, varinfo=varinfo
    )
    task.run()
    assert task.output_values == {"return_value": {"result": 8}}


def test_unpacked_method_task(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    task = Task.instantiate(
        "UnpackedMethodExecutorTask",
        method=qualname(mymethod),
        a=3,
        b=5,
        output_names=["result"],
        varinfo=varinfo,
    )
    task.run()
    assert task.output_values == {"result": 8}


shellscript = r"""a=0

while getopts u:a:f: flag
do
    case "${flag}" in
        a) a=${OPTARG};;
    esac
done

echo $a
if [[ $a == "10" ]]; then
    exit 0
else
    exit 1
fi
"""

pyscript = r"""
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--a", type=int, default=0)
    args = parser.parse_args()
    print(args.a)
    assert args.a == 10
"""


def test_python_script_task(tmpdir, capsys):
    varinfo = {"root_uri": str(tmpdir)}
    pyscriptname = tmpdir / "test.py"
    with open(pyscriptname, mode="w") as f:
        f.writelines(pyscript)

    task = Task.instantiate(
        "ScriptExecutorTask", script=str(pyscriptname), a=10, varinfo=varinfo
    )
    task.run()
    assert task.output.returncode.value == 0
    captured = capsys.readouterr()
    # assert captured.out == "10\n"
    assert captured.err == ""


def test_shell_script_task(tmpdir, capsys):
    varinfo = {"root_uri": str(tmpdir)}
    shellscriptname = tmpdir / "test.sh"
    with open(shellscriptname, mode="w") as f:
        f.writelines(shellscript)
    os.chmod(shellscriptname, 0o755)

    task = Task.instantiate(
        "ScriptExecutorTask", script=str(shellscriptname), a=10, varinfo=varinfo
    )
    task.run()
    assert task.output.returncode.value == 0
    captured = capsys.readouterr()
    # assert captured.out == "10\n"
    assert captured.err == ""
