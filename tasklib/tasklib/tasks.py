from esrftaskgraph import Task


__all__ = ["SumTask", "CondSumTask"]


class SumTask(Task):
    INPUT_NAMES = ["a"]
    OPTIONAL_INPUT_NAMES = ["b"]
    OUTPUT_NAMES = ["result"]

    def process(self):
        result = self.input.a.value
        if self.input.b.value is not None:
            result += self.input.b.value
        self.output.result.value = result


class CondSumTask(SumTask):
    OUTPUT_NAMES = SumTask.OUTPUT_NAMES + ["too_small"]

    def process(self):
        super().process()
        self.output.too_small.value = self.output.result.value < 10


class ErrorSumTask(Task):
    OPTIONAL_INPUT_NAMES = ["a", "b", "raise_error"]
    OUTPUT_NAMES = ["result"]

    def process(self):
        result = self.input.a.value
        if result is None:
            result = 0
        if self.input.b.value is not None:
            result += self.input.b.value
        self.output.result.value = result
        if self.input.raise_error.value:
            raise RuntimeError("Intentional error")
