import sys
import time
import socket
import subprocess
import requests
import pytest


def get_free_ports(n):
    sockets = [socket.socket() for _ in range(n)]
    try:
        for s in sockets:
            s.bind(("", 0))
        return [s.getsockname()[1] for s in sockets]
    finally:
        for s in sockets:
            s.close()


def eprint(*args):
    print(*args, file=sys.stderr, flush=True)


def terminate_process(process):
    cmd = repr(" ".join(process.args))
    if process.poll() is not None:
        eprint(f"Process {cmd} already terminated with code {process.returncode}")
        return
    process.terminate()
    process.wait(timeout=10)
    if process.poll() is not None:
        return
    eprint(f"Process {cmd} doesn't finish: try to kill it ...")
    process.kill()
    process.wait(timeout=10)
    if process.poll() is not None:
        return
    raise RuntimeError(f"Process {cmd} cannot be killed ...")


def wait_online(process, url, timeout=10, period=0.5):
    t0 = time.time()
    while True:
        try:
            requests.get(url)
            break
        except Exception:
            if process.poll() is None:
                if time.time() - t0 > timeout:
                    raise TimeoutError(url + " is not responding")
                time.sleep(period)
            else:
                raise RuntimeError("luigid is not running") from None


@pytest.fixture()
def luigid(tmpdir):
    # Centralized scheduler for Luigi
    rootdir = tmpdir / "luigid"
    logdir = rootdir / "logs"
    pidfile = rootdir / "luigid.pid"
    statefile = rootdir / "state.pickle"
    port = get_free_ports(1)[0]

    rootdir.mkdir()
    logdir.mkdir()

    args = [
        "luigid",
        "--pidfile",
        str(pidfile),
        "--logdir",
        str(logdir),
        "--state-path",
        str(statefile),
        "--port",
        str(port),
    ]
    process = subprocess.Popen(args)
    try:
        wait_online(process, f"http://localhost:{port}")
        yield port
    finally:
        terminate_process(process)
