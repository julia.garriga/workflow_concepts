import sys
import logging
from esrf2luigi import job
from taskgraphlib import taskgraphs
from taskgraphlib import check_pipeline

logging.getLogger("esrf2luigi").setLevel(logging.DEBUG)
logging.getLogger("esrf2luigi").addHandler(logging.StreamHandler(sys.stdout))


def test_job(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    scheduler = {"log_level": "DEBUG"}
    job(graph, varinfo=varinfo, scheduler=scheduler)
    check_pipeline(graph, expected, varinfo)


def test_job_centralized(tmpdir, luigid):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    scheduler = {
        "local_scheduler": False,
        "log_level": "DEBUG",
        "scheduler_port": luigid,
    }
    job(
        graph,
        varinfo=varinfo,
        scheduler=scheduler,
    )
    check_pipeline(graph, expected, varinfo)
