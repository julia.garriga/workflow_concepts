from paradag import DAG
from paradag import dag_run
from paradag import MultiThreadProcessor
from paradag import CallableExecutor
from paradag import FullSelector
from esrftaskgraph import load_graph


def convert_graph(esrfgraph, varinfo):
    pipeline = DAG()
    tasks = dict()
    for node in esrfgraph.graph.nodes:
        task = esrfgraph.instantiate_task_static(node, tasks=tasks, varinfo=varinfo)
        pipeline.add_vertex(task.run)
    for node in esrfgraph.graph.nodes:
        task = tasks[node]
        for upstream in esrfgraph.predecessors(node):
            pipeline.add_edge(tasks[upstream].run, task.run)
    return pipeline


def job(graph, representation=None, varinfo=None):
    esrfgraph = load_graph(source=graph, representation=representation)
    if esrfgraph.is_cyclic:
        raise RuntimeError("paradag can only execute DAGs")
    pipeline = convert_graph(esrfgraph, varinfo)
    dag_run(
        pipeline,
        selector=FullSelector(),
        processor=MultiThreadProcessor(),
        executor=CallableExecutor(),
    )
