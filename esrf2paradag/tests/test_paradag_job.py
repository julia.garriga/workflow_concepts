import sys
import logging
from esrf2paradag import job
from taskgraphlib import taskgraphs
from taskgraphlib import check_pipeline

logging.getLogger("paradag").setLevel(logging.DEBUG)
logging.getLogger("paradag").addHandler(logging.StreamHandler(sys.stdout))
logging.getLogger("esrf2paradag").setLevel(logging.DEBUG)
logging.getLogger("esrf2paradag").addHandler(logging.StreamHandler(sys.stdout))


def test_job(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    job(graph, varinfo=varinfo)
    check_pipeline(graph, expected, varinfo)
